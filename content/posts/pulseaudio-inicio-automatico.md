---
title: "Solución: Pulseaudio no inicia de forma automática"
date: 2021-01-29T05:13:50-05:00
draft: false
---

La solución a este problema lo encontré en reddit, posteriormente lo publique en los foros de Devuan, el usuario dice que el problema le surgió en Debian Unstable, y aquí les dejo la solución que también me funciono:

  

Mira si tienes un archivo llamado:  

    /etc/pulse/client.conf.d/00-disable-autospawn.conf

  

Si hay una línea que dice:

    autospawn=no

  

comentarla con punto y coma:

    ;autospawn=no

  

  

Alternativamente, comprueba si en /etc/pulse/client.conf o tu directorio de configuración de inicio autospawn está desactivado, y cambia eso.


fuente:

  

[http://dev1galaxy.org/viewtopic.php?id=3285](http://dev1galaxy.org/viewtopic.php?id=3285)
