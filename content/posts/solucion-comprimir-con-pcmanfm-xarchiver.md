---
title: "Solución al comprimir con Pcmanfm Xarchiver"
date: 2021-01-29T05:04:08-05:00
draft: false
---

Si al intentar comprimir archivos con PCManFM usando Xarchiver no funciona la solución es cambiar un parámetro del archivo "archivers.list" de la librería libfm.  
  

Seria de la siguiente forma:

  

escribimos en la terminal:  

  

	sudo nano /usr/share/libfm/archivers.list

  
luego buscar donde dice:  
  

	xarchiver --add-to %F a 

  
y cambiarlo a:  
  

	xarchiver  --compress %F

  
  
Guárdalo y listo. Con esto funcionaria la opción de comprimir en PCManFM con Xarchiver. Espero les sea de utilidad.
