---
title: "Solución: Pantalla negra antes de iniciar Lightdm Runit"
date: 2021-01-29T05:17:05-05:00
draft: false
---

Resulta que por alguna razón Lightdm tiene conflicto con la tty1 y entonces cuando intenta iniciar lightdm no aparece nada ni mouse no se puede ingresar a la tty, solo apagar con el botón de encendido/apagado.

Este problema lo encontré en Artix Linux usando el init Runit con gráficos Nvidia. Tenia la misma distribución en mi netbook con intel y no sucedió eso, pero bueno me funciono esta solución:

Edita el script de lightdm que esta dentro de los servicios de runit:

  

    sudo nano /run/runit/service/lightdm/run

Si la ubicación esta bien puesta te debe aparecer lo siguiente dentro del script run:

    #!/bin/sh
    sv check dbus >/dev/null || exit 1
    install -d -m0711 -olightdm -glightdm /run/lightdm
    exec lightdm

  

Agrégale "**sleep 1**" debajo de "**sv check dbus >/dev/null || exit 1**", Quedaría así:

  

    #!/bin/sh
    sv check dbus >/dev/null || exit 1
    sleep 1
    install -d -m0711 -olightdm -glightdm /run/lightdm
    exec lightdm

  

Fuente donde encontré esa solución:  

[https://forum.artixlinux.org/index.php/topic,1883.msg12901.html#msg12901](https://forum.artixlinux.org/index.php/topic,1883.msg12901.html#msg12901)
