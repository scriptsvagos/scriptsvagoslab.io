---
title: "Disminuir ruido de microfono con Pulseaudio"
date: 2021-01-29T04:31:19-05:00
draft: false
---

Solución para disminuir el ruido de micrófonos en distribuciones GNU/Linux  
  
  
  
Acceder al siguiente fichero:  
  

    sudo nano /etc/pulse/default.pa

  
Dentro del fichero agregar lo siguiente:  
  

    ### Enable Echo/Noise-Cancellation
    load-module module-echo-cancel aec_method=webrtc aec_args="analog_gain_control=0\ digital_gain_control=0" source_name=microHD sink_name=microHD source_properties=device.description=MicroHD
    set-default-source MicroHD
    set-default-sink MicroHD

Luego de guardar cierre sesión e inicie otra vez para que se efectúen los cambios.
