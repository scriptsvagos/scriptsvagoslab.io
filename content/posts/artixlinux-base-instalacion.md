---
title: "Guia de Instalación para Artix Linux Base(runit)"
date: 2021-01-29T12:06:07-05:00
draft: false
---
Esta guía es solo para instalar Artix Base con el init Runit.

Empezamos!.
---

Para ver las distribuciones de teclados:

    ls -R /usr/share/kbd/keymaps

en mi caso elegiré `us` por el teclado que tengo:

    loadkeys us


Particiones
---
Para ver las particiones disponibles utilizaremos `lsblk`, luego de ver que dispositivo de almacenamiento utilizaremos cfdisk, en mi caso sda:

    cfdisk /dev/sda

Nos aparecerán varias opciones, tal como gpt, msdos. Si estas en una instalación UEFI selecciona GPT, en LEGACY selecciona msdos.

*Si elegiste GPT, es necesario crear una partición /boot, lo cual la pagina oficial sugiere 512MB de espacio para esta partición. En el caso de legacy no es necesario crear dicha partición.*
<br>
**El orden seria:**

 - `/boot` de 512M
 - `/` de el espacio que le des, en mi caso x ejemplo 248G
 - `SWAP` de 2G

Una vez hecha las particiones guardaras los cambios.

<br>

Para ubicar las particiones que hiciste, volverás a ejecutar el comando `lsblk` y veras las particiones en el disco que elegiste (en mi caso sda).

Habrá una ordenación de las demás particiones que creamos dentro de ella, en mi caso quedo así:

sda

├─sda1 512MB

├─sda2 248G

└─sda3 2G

<br>

**Vamos a darle formato:**

**sda1(boot UEFI) es:** 

    mkfs.fat -F 32 /dev/sda1

<br>

**sda2(/) es:** 

    mkfs.ext4 /dev/sda2

<br>

**sda3(SWAP) es:** 

    mkswap /dev/sda3

<br>

**Aquí procederemos a montar las particiones:**

  

La primera es la SWAP, en mi caso es la sda3

    swapon /dev/sda3

<br>
Siguente la raiz(/) para luego crear la carpeta boot y montar la partición /boot:

    mount /dev/sda2 /mnt
    mkdir /mnt/boot
    mount /dev/sda1 /mnt/boot

<br>
  
Conexión de Internet
--
Testearemos la conexión de internet
En caso sea cableada solo tendremos que poner: `ping artixlinux.org`

  

En el caso de redes WIFI se utilizara `connmanctl`:

    connmanctl
    enable wifi
    agent on (para poder mostrar las redes que estan protegidas)
    scan wifi
    services (mostrara las redes escaneadas)
    connect wifi_ (luego del guion bajo presiona la tecla TAB para autocompletar los numeros de la red que elegiste)

  

luego que la conexión haya sido exitosa sales con `quit`
<br>

Instalar el sistema base:
---
  

    basestrap /mnt base base-devel(opcional pero lo recomiendo) runit elogind-runit

<br>

**Instalación del kernel:**

**Para linux:**

    basestrap /mnt linux linux-firmware(para que detecte tarjetas wifi, etc, recomendado.)

  
  

**Para linux-lts:**

    basestrap /mnt linux-lts linux-firmware(para que detecte tarjetas wifi, etc, recomendado.)


Generaremos el Fstab de las particiones:

    fstabgen -U /mnt >> /mnt/etc/fstab
    

Vamos a entrar al sistema para terminar las ultimas configuraciones con:

    artix-chroot /mnt

<br>

**Configurar la zona horaria:**

  Para buscar nuestra zona horaria:

    ls /usr/share/zoneinfo

elegiré América y país Panamá. En mi caso quedaría así:

    ln -sf /usr/share/zoneinfo/America/Panama /etc/localtime

  
Ya por ultimo para generar la configuración de la zona horaria:

    hwclock --systohc
<br>

**Configurar la localización:**

    pacman -S nano
    nano /etc/locale.gen

Dentro de este archivo debemos descomentar el `UTF` e `ISO` de nuestro idioma/país. en mi caso:

    es_PA.UTF-8 UTF-8  
    es_PA ISO-8859-1

Guardamos y luego generaremos los cambios:

    locale-gen

  
  
Para hacer permanente el idioma de teclado:

    nano /etc/vconsole.conf

dentro escribir el idioma de teclado que quieren, en mi caso:

    KEYMAP=us

guardan y cierran el archivo.
<br>
Estableceremos la configuración regional en todo el sistema:

    nano /etc/locale.conf

dentro elegiremos el idioma que teniamos, en mi caso es_PA.UTF-8, quedaría así:

    LANG=es_PA.UTF-8

<br>

**Instalaremos el Boot loader:**

    pacman -S grub os-prober(en caso tengan mas sistemas) efibootmgr(solo para UEFI)
 
 
**En caso sea LEGACY:**

    grub-install --recheck -dev/sda

  
**En caso sea UEFI:**

    grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub (en caso sea UEFI)

Por ultimo:

    grub-mkconfig -o /boot/grub/grub.cfg

  
  

Si queremos agregarle contraseña al usuario `root` seria:

    passwd (luego les pedira la contraseña, que la repitan, y listo)

En mi caso utilizare un usuario nuevo que tenga esos privilegios, entonces seria lo siguiente:

    nano /etc/sudoers

dentro del archivo sudoers descomentaremos el parámetro `#%wheel ALL=(ALL) ALL`, quedando así:

    %wheel ALL=(ALL) ALL

guardamos y cerramos del archivo.
<br>

Ahora crearemos el nuevo usuario con privilegios administrador de la siguiente manera:

    useradd -mG wheel tunombredeusuario
    passwd tunombredeusuario

  
  
  

Vamos Asignarle un nombre al PC:

    nano /etc/hostname

dentro escribiremos el nombre que queremos para el pc, en mi caso:

    potato

guardamos y cerramos el archivo.
<br>



Instalaremos `connman`(gestor de conexiones)

    pacman -S connman-runit connman-gtk
    ln -s /etc/runit/sv/connmand /etc/runit/runsvdir/default

<br>

solo queda desmontar la partición:

    exit
    umount -R /mnt

  

ya con esto tenemos el sistema base instalado.

**Reinicias y listo!, ya tienes artix linux base instalado!.**

<br>

En el caso de la post-instalación bspwm(opcional)
---
Utilizare mi script de artixbsp(bspwm).

**Inicia sesion, y consta de lo siguente:**

    sudo pacman -S git
    git clone https://gitlab.com/d33vliter/artixbsp
    
Luego ejecutaremos el script sin sudo(ya dentro pedirá contraseña, si haces lo contrario la instalación se instalara en root)
 

     cd artixbsp
     ./artixbsp
     cd (ejecutalo cuando finalize la instalación)


Una vez concluya la instalación tendrás que instalar `dbus-runit`(el script como es para cualquier init no le agregue el `dbus-runit`):

    sudo pacman -S dbus-runit
    sudo ln -s /etc/runit/sv/dbus /run/runit/service/dbus

  

el script Artixbsp no contiene gestor de inicio de sesión por lo cual iniciaremos el entorno gráfico con:

    startx

<br>
Espero les haya gustado esta guía de instalación escrita.


Referencias: 
https://wiki.artixlinux.org/Main/Installation
https://wiki.artixlinux.org/Main/Runit
