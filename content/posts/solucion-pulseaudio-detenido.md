---
title: "Solución: Pulseaudio se detiene al no utilizarse"
date: 2021-01-29T05:22:51-05:00
draft: false
---

Pulseaudio al utilizar gestores de ventana al parecer por defecto cuando no se utiliza un programa que necesite de el, se detiene el proceso. Esto sucede por lo general en los gestores de ventanas(i3, bspwm, openbox, etc) no en entornos de escritorio(xfce, mate, kde).

Entonces la solución básicamente es esta:  

Entrar a:  

    /etc/pulse/daemon.conf 

Si tu configuracion de pulse audio esta tu carpeta HOME entonces ir a:

    $HOME/.config/pulse/daemon.conf

Buscar la linea que dice:

    ;exit-idle-time = 20

descomentar y cambiarla por:

    exit-idle-time = -1 

Esto significa que Pulseaudio cuando inicie la sesión se mantendrá ejecutándose y finalizara al cerrar sesión.  

Igual recuerden agregar al autostart de su gestor de ventanas el proceso de PulseaudioX11, así:

    start-pulseaudio-x11 &

Esto seria todo.
