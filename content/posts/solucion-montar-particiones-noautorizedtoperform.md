---
title: "Solución al montar particiones: Not authorized to perform operation"
date: 2021-01-29T05:08:44-05:00
draft: false
---

Lo probé con PcManFM, igual testear en otro gestor de archivos en caso no les funcione.  
  
  
En la terminal poner lo siguiente:  
  

    sudo nano /etc/polkit-1/localauthority/50-local.d/50-filesystem-mount-system-internal.pkla

  
  
dentro escribir:  
  

    [Mount a system-internal device]
    Identity=*
    Action=org.freedesktop.udisks2.*
    ResultActive=yes

  
  
Guardar y listo.
